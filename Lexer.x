{
module Lexer  where
import DataTypes (Token(..))
}

%wrapper "basic"


$digit = 0-9            -- digits
$alpha = [a-zA-Z]       -- alphabetic characters
$alphaNum = [a-zA-Z0-9] -- Alphanumberic characters

tokens :-

    $white+                                         ;
    "--".*                                          ;
    alive                                           { \s -> AliveT }
    unded                                           { \s -> UndedT }    
    $digit+                                         { \s -> IntT (read s) }
    "0f"[0-5]+                                      { \s -> IntT (baseFiveToTen (read (remove0f s))) }
    $digit*\.$digit+                                { \s -> FloatT (read s) }
    "Yas"                                           { \s -> TrueT }
    Y                                               { \s -> TrueT }
    Nah                                             { \s -> FalseT }
    N                                               { \s -> FalseT }
    tau                                             { \s -> FloatT 6.28318 }
    e                                               { \s -> FloatT 2.71828 }
    5ive                                            { \s -> FloatT 5.55555 }
    ":D"                                            { \s -> UnitT }
    "["                                             { \s -> LBracketT }
    ","                                             { \s -> CommaT }
    "]"                                             { \s -> RBracketT }
    "1st"                                           { \s -> FstT }
    "2nd"                                           { \s -> SndT }
    "not"                                           { \s -> NotT }
    "("                                             { \s -> LParenT }
    ")"                                             { \s -> RParenT }
    "fun"                                           { \s -> FunctT }
    "->"                                            { \s -> ArrowT }
    if                                              { \s -> IfT }
    then                                            { \s -> ThenT }
    else                                            { \s -> ElseT }
    ive                                             { \s -> IveT }
    in                                              { \s -> InT } 
    "+"                                             { \s -> PlusT }
    "-"                                             { \s -> MinusT }
    "*"                                             { \s -> MultT }
    "/"                                             { \s -> DivT }
    "//"                                            { \s -> IntDivT }
    "%"                                             { \s -> ModT }
    "^"                                             { \s -> ExponentT }
    and                                             { \s -> AndT }
    or                                              { \s -> OrT }
    "<"                                             { \s -> LtT }
    "<="                                            { \s -> LtEqT }
    ">"                                             { \s -> GtT }
    ">="                                            { \s -> GtEqT }
    "="                                             { \s -> EqT }
    "!="                                            { \s -> NEqT }
    \"$alphaNum$alphaNum+\"                         { \s -> StringT (removeQuote s) }
    $alphaNum [$alpha $digit \_ \']*                { \s -> VarT s}
    "lit"                                           { \s -> LitT }
    while                                           { \s -> WhileT }
    do                                              { \s -> DoT }
    done                                            { \s -> DoneT }
    "@="                                            { \s -> AssignT }
    "#"                                             { \s -> DeLitT }
    "..."					                        { \s -> ElipsisT }

{
--  ';'                                             { \s -> ConsT }
  
-- Each action has type :: String -> Token

--Removes quotes for string parsing
removeQuote :: String -> String
removeQuote ('\"':xs) = removeQuote xs
removeQuote (x:xs) = [x] ++ (removeQuote xs)
removeQuote "" = ""

--Removes the '0f' proceeding base five values
remove0f :: String -> String
remove0f ('0':'f':xs) = xs
remove0f x = x

baseTenToFive :: Integer -> Integer
baseTenToFive 0 = 0
baseTenToFive x = (x `mod` 5) + (10 * (baseTenToFive (x `div` 5)))

baseFiveToTen :: Integer -> Integer
baseFiveToTen x = aux x 0 
                  where aux 0 acc = 0
                        aux x acc = ((x `mod` 10) * (5 ^ acc)) + (aux (x `div` 10) (acc+1))

}
