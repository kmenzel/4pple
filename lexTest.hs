import Lexer
import Data.Maybe


inputs = ["5",
          "52",
          "0f34322",
          "172.2",
          "7*3",
          "MR",
          "MS",
          "e",
          "tau",
          "ifz",
          "then",
          "else",
          "+ * / -",
          "ifz 3 then 35.23 else 10",
          "7 - 2 35.23 // tau",
          "76 - (7 * 10) - 6 MS",
          "ifz MR then 32 else 5ive",
          "else ifz then MR MS - 0.0",
          "0.0 0 0.9 1.9 1.5"
          ]

pad str n = 
    let spaces = ' ':spaces
        padding = take (n - length(str)) spaces
    in str++padding


scanTokens :: String -> Maybe [Token]
scanTokens str = go ('\n',[],str)
  where go inp@(_,_bs,str) =
          case alexScan inp 0 of
                AlexEOF -> Just []
                AlexError _ -> Nothing
                AlexSkip  inp' len     -> go inp'
                AlexToken inp' len act -> fmap ((act (take len str)):) (go inp')

testStr n str = 
    let resStr = show $ scanTokens str in
    do
      putStr (pad str n)
      putStrLn resStr

main = 
    let ml = (maximum (map length inputs)) + 4
        tests = map (testStr ml) inputs
    in do
      putStrLn "0f34322 should be 2462 in base 10."
      sequence tests
      return ()
