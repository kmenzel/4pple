module EvalBinOp where

import Data.Fixed (mod')
import DataTypes (Value(..), BinOp(..))

evalBinOp :: BinOp -> Value -> Value -> Maybe Value
evalBinOp op a b = case op of PlusOp -> arithOp (+) (+) a b
                              MinusOp -> arithOp (-) (-) a b
                              MultOp -> arithOp (*) (*) a b
                              ModOp -> arithOp mod mod' a b
                              ExponentOp -> arithOp (^) (**) a b
                              AndOp -> boolOp (&&) a b
                              OrOp -> boolOp (||) a b
                              LtOp -> ordOp (<) (<) a b
                              LtEqOp -> ordOp (<=) (<=) a b
                              GtOp -> ordOp (>) (>) a b
                              GtEqOp -> ordOp (>=) (>=) a b
                              DivOp -> divV a b
                              IntDivOp -> iDivV a b
                              EqOp -> Just $ BoolV $ a == b -- EqOp and NEqOp have to be able to check equality of any type and thus do not return nothing
                              NEqOp -> Just $ BoolV $ a /= b

divV :: Value -> Value -> Maybe Value --divV cannot use an OP fuction due to its constant output type 
divV (IntV x) (IntV y) = Just $ RealV $ (fromInteger x) / (fromInteger y)
divV (RealV x) (IntV y) = Just $ RealV $ x / (fromInteger y)
divV (IntV x) (RealV y) = Just $ RealV $ (fromInteger x) / y
divV (RealV x) (RealV y) = Just $ RealV $ x / y
divV _ _ = Nothing

iDivV :: Value -> Value -> Maybe Value --iDivV cannot use an OP fuction due to its constant output type  
iDivV (IntV x) (IntV y) = Just $ IntV $ x `div` y
iDivV (RealV x) (IntV y) = Just $ IntV $ (truncate x) `div` y
iDivV (IntV x) (RealV y) = Just $ IntV $ x `div` (truncate y)
iDivV (RealV x) (RealV y) = Just $ IntV $ (truncate x) `div` (truncate y)
iDivV _ _ = Nothing

--Boolean Operators
boolOp :: (Bool -> Bool -> Bool) -> Value  -> Value -> Maybe Value
boolOp boolFunct (BoolV x) (BoolV y) = Just $ BoolV $ boolFunct x y
boolOp _ _ _ = Nothing

--Relational Operators
ordOp :: (Integer -> Integer -> Bool) -> (Float -> Float -> Bool) -> Value -> Value -> Maybe Value
ordOp intFunct _ (IntV x) (IntV y) = Just $ BoolV $ intFunct x y
ordOp _ floatFunct (RealV x) (IntV y) = Just $ BoolV $ floatFunct x (fromIntegral y)
ordOp _ floatFunct (IntV x) (RealV y) = Just $ BoolV $ floatFunct (fromIntegral x) y
ordOp _ floatFunct (RealV x) (RealV y) = Just $ BoolV $ floatFunct x y
ordOp _ _ _ _ = Nothing

--Aritmetic Operators
arithOp :: (Integer -> Integer -> Integer) -> (Float -> Float -> Float) -> Value -> Value -> Maybe Value
arithOp intFunct floatFunct (IntV x) (IntV y) = Just $ IntV $ intFunct x y
arithOp intFunct floatFunct (IntV x) (RealV y) = Just $ RealV $ floatFunct (fromInteger x) y
arithOp intFunct floatFunct (RealV x) (IntV y) = Just $ RealV $ floatFunct x (fromInteger y)
arithOp intFunct floatFunct (RealV x) (RealV y) = Just $ RealV $ floatFunct  x y
arithOp _ _ _ _ = Nothing


