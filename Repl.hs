import Lexer
import Parser
import Eval
import DataTypes
import Contexts
import System.IO
import System.Environment

main :: IO ()
main = repl (Context [] [])

repl :: Context -> IO ()
repl context = do
                   putStr "4pple> "
                   hFlush stdout
                   line <- getBlock
                   let stmt = case (parse (alexScanTokens line)) of
                                    Just x -> x
                                    Nothing -> ExpressionStmt $ StringExp "Parse Error."
                   let (out, newContext) = case (evalS context stmt) of
                                            Just (UnitV, retContext) -> ("", retContext)
                                            Just (val, retContext) -> ((show val) ++ "\n", retContext)
                                            Nothing -> ("Evaluation Error.\n", context)
                   putStr out
                   repl newContext

getBlock :: IO String
getBlock =
    let aux acc = do
        line <- getLine
        let acc' = acc ++ " " ++ line
            toks = alexScanTokens acc'
            ast = parse toks
        case (line, ast) of
            ("", _) -> return acc'
            (_, Nothing) -> aux acc'
            _ -> return acc'
    in aux ""
