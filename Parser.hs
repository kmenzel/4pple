{-# OPTIONS_GHC -w #-}
module Parser where
import Lexer
import DataTypes
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6

action_0 (7) = happyShift action_23
action_0 (8) = happyShift action_24
action_0 (9) = happyShift action_3
action_0 (10) = happyShift action_4
action_0 (11) = happyShift action_5
action_0 (12) = happyShift action_6
action_0 (13) = happyShift action_7
action_0 (14) = happyShift action_8
action_0 (15) = happyShift action_9
action_0 (16) = happyShift action_10
action_0 (19) = happyShift action_11
action_0 (20) = happyShift action_12
action_0 (21) = happyShift action_13
action_0 (22) = happyShift action_14
action_0 (24) = happyShift action_15
action_0 (26) = happyShift action_16
action_0 (29) = happyShift action_17
action_0 (32) = happyShift action_18
action_0 (46) = happyShift action_19
action_0 (47) = happyShift action_20
action_0 (51) = happyShift action_21
action_0 (4) = happyGoto action_22
action_0 (5) = happyGoto action_2
action_0 _ = happyFail

action_1 (9) = happyShift action_3
action_1 (10) = happyShift action_4
action_1 (11) = happyShift action_5
action_1 (12) = happyShift action_6
action_1 (13) = happyShift action_7
action_1 (14) = happyShift action_8
action_1 (15) = happyShift action_9
action_1 (16) = happyShift action_10
action_1 (19) = happyShift action_11
action_1 (20) = happyShift action_12
action_1 (21) = happyShift action_13
action_1 (22) = happyShift action_14
action_1 (24) = happyShift action_15
action_1 (26) = happyShift action_16
action_1 (29) = happyShift action_17
action_1 (32) = happyShift action_18
action_1 (46) = happyShift action_19
action_1 (47) = happyShift action_20
action_1 (51) = happyShift action_21
action_1 (5) = happyGoto action_2
action_1 _ = happyFail

action_2 (22) = happyShift action_40
action_2 (31) = happyShift action_41
action_2 (32) = happyShift action_42
action_2 (33) = happyShift action_43
action_2 (34) = happyShift action_44
action_2 (35) = happyShift action_45
action_2 (36) = happyShift action_46
action_2 (37) = happyShift action_47
action_2 (38) = happyShift action_48
action_2 (39) = happyShift action_49
action_2 (40) = happyShift action_50
action_2 (41) = happyShift action_51
action_2 (42) = happyShift action_52
action_2 (43) = happyShift action_53
action_2 (44) = happyShift action_54
action_2 (45) = happyShift action_55
action_2 (50) = happyShift action_56
action_2 (52) = happyShift action_57
action_2 (6) = happyGoto action_39
action_2 _ = happyReduce_1

action_3 _ = happyReduce_5

action_4 _ = happyReduce_4

action_5 _ = happyReduce_6

action_6 _ = happyReduce_7

action_7 _ = happyReduce_8

action_8 _ = happyReduce_9

action_9 _ = happyReduce_10

action_10 (9) = happyShift action_3
action_10 (10) = happyShift action_4
action_10 (11) = happyShift action_5
action_10 (12) = happyShift action_6
action_10 (13) = happyShift action_7
action_10 (14) = happyShift action_8
action_10 (15) = happyShift action_9
action_10 (16) = happyShift action_10
action_10 (19) = happyShift action_11
action_10 (20) = happyShift action_12
action_10 (21) = happyShift action_13
action_10 (22) = happyShift action_14
action_10 (24) = happyShift action_15
action_10 (26) = happyShift action_16
action_10 (29) = happyShift action_17
action_10 (32) = happyShift action_18
action_10 (46) = happyShift action_19
action_10 (47) = happyShift action_20
action_10 (51) = happyShift action_21
action_10 (5) = happyGoto action_38
action_10 _ = happyFail

action_11 (22) = happyShift action_37
action_11 _ = happyFail

action_12 (22) = happyShift action_36
action_12 _ = happyFail

action_13 (9) = happyShift action_3
action_13 (10) = happyShift action_4
action_13 (11) = happyShift action_5
action_13 (12) = happyShift action_6
action_13 (13) = happyShift action_7
action_13 (14) = happyShift action_8
action_13 (15) = happyShift action_9
action_13 (16) = happyShift action_10
action_13 (19) = happyShift action_11
action_13 (20) = happyShift action_12
action_13 (21) = happyShift action_13
action_13 (22) = happyShift action_14
action_13 (24) = happyShift action_15
action_13 (26) = happyShift action_16
action_13 (29) = happyShift action_17
action_13 (32) = happyShift action_18
action_13 (46) = happyShift action_19
action_13 (47) = happyShift action_20
action_13 (51) = happyShift action_21
action_13 (5) = happyGoto action_35
action_13 _ = happyFail

action_14 (9) = happyShift action_3
action_14 (10) = happyShift action_4
action_14 (11) = happyShift action_5
action_14 (12) = happyShift action_6
action_14 (13) = happyShift action_7
action_14 (14) = happyShift action_8
action_14 (15) = happyShift action_9
action_14 (16) = happyShift action_10
action_14 (19) = happyShift action_11
action_14 (20) = happyShift action_12
action_14 (21) = happyShift action_13
action_14 (22) = happyShift action_14
action_14 (24) = happyShift action_15
action_14 (26) = happyShift action_16
action_14 (29) = happyShift action_17
action_14 (32) = happyShift action_18
action_14 (46) = happyShift action_19
action_14 (47) = happyShift action_20
action_14 (51) = happyShift action_21
action_14 (5) = happyGoto action_34
action_14 _ = happyFail

action_15 (14) = happyShift action_33
action_15 _ = happyFail

action_16 (9) = happyShift action_3
action_16 (10) = happyShift action_4
action_16 (11) = happyShift action_5
action_16 (12) = happyShift action_6
action_16 (13) = happyShift action_7
action_16 (14) = happyShift action_8
action_16 (15) = happyShift action_9
action_16 (16) = happyShift action_10
action_16 (19) = happyShift action_11
action_16 (20) = happyShift action_12
action_16 (21) = happyShift action_13
action_16 (22) = happyShift action_14
action_16 (24) = happyShift action_15
action_16 (26) = happyShift action_16
action_16 (29) = happyShift action_17
action_16 (32) = happyShift action_18
action_16 (46) = happyShift action_19
action_16 (47) = happyShift action_20
action_16 (51) = happyShift action_21
action_16 (5) = happyGoto action_32
action_16 _ = happyFail

action_17 (14) = happyShift action_31
action_17 _ = happyFail

action_18 (9) = happyShift action_3
action_18 (10) = happyShift action_4
action_18 (11) = happyShift action_5
action_18 (12) = happyShift action_6
action_18 (13) = happyShift action_7
action_18 (14) = happyShift action_8
action_18 (15) = happyShift action_9
action_18 (16) = happyShift action_10
action_18 (19) = happyShift action_11
action_18 (20) = happyShift action_12
action_18 (21) = happyShift action_13
action_18 (22) = happyShift action_14
action_18 (24) = happyShift action_15
action_18 (26) = happyShift action_16
action_18 (29) = happyShift action_17
action_18 (32) = happyShift action_18
action_18 (46) = happyShift action_19
action_18 (47) = happyShift action_20
action_18 (51) = happyShift action_21
action_18 (5) = happyGoto action_30
action_18 _ = happyFail

action_19 (9) = happyShift action_3
action_19 (10) = happyShift action_4
action_19 (11) = happyShift action_5
action_19 (12) = happyShift action_6
action_19 (13) = happyShift action_7
action_19 (14) = happyShift action_8
action_19 (15) = happyShift action_9
action_19 (16) = happyShift action_10
action_19 (19) = happyShift action_11
action_19 (20) = happyShift action_12
action_19 (21) = happyShift action_13
action_19 (22) = happyShift action_14
action_19 (24) = happyShift action_15
action_19 (26) = happyShift action_16
action_19 (29) = happyShift action_17
action_19 (32) = happyShift action_18
action_19 (46) = happyShift action_19
action_19 (47) = happyShift action_20
action_19 (51) = happyShift action_21
action_19 (5) = happyGoto action_29
action_19 _ = happyFail

action_20 (9) = happyShift action_3
action_20 (10) = happyShift action_4
action_20 (11) = happyShift action_5
action_20 (12) = happyShift action_6
action_20 (13) = happyShift action_7
action_20 (14) = happyShift action_8
action_20 (15) = happyShift action_9
action_20 (16) = happyShift action_10
action_20 (19) = happyShift action_11
action_20 (20) = happyShift action_12
action_20 (21) = happyShift action_13
action_20 (22) = happyShift action_14
action_20 (24) = happyShift action_15
action_20 (26) = happyShift action_16
action_20 (29) = happyShift action_17
action_20 (32) = happyShift action_18
action_20 (46) = happyShift action_19
action_20 (47) = happyShift action_20
action_20 (51) = happyShift action_21
action_20 (5) = happyGoto action_28
action_20 _ = happyFail

action_21 (9) = happyShift action_3
action_21 (10) = happyShift action_4
action_21 (11) = happyShift action_5
action_21 (12) = happyShift action_6
action_21 (13) = happyShift action_7
action_21 (14) = happyShift action_8
action_21 (15) = happyShift action_9
action_21 (16) = happyShift action_10
action_21 (19) = happyShift action_11
action_21 (20) = happyShift action_12
action_21 (21) = happyShift action_13
action_21 (22) = happyShift action_14
action_21 (24) = happyShift action_15
action_21 (26) = happyShift action_16
action_21 (29) = happyShift action_17
action_21 (32) = happyShift action_18
action_21 (46) = happyShift action_19
action_21 (47) = happyShift action_20
action_21 (51) = happyShift action_21
action_21 (5) = happyGoto action_27
action_21 _ = happyFail

action_22 (53) = happyAccept
action_22 _ = happyFail

action_23 (14) = happyShift action_26
action_23 _ = happyFail

action_24 (14) = happyShift action_25
action_24 _ = happyFail

action_25 (44) = happyShift action_71
action_25 _ = happyFail

action_26 (44) = happyShift action_70
action_26 _ = happyFail

action_27 (22) = happyShift action_40
action_27 (31) = happyShift action_41
action_27 (32) = happyShift action_42
action_27 (33) = happyShift action_43
action_27 (34) = happyShift action_44
action_27 (35) = happyShift action_45
action_27 (36) = happyShift action_46
action_27 (37) = happyShift action_47
action_27 (38) = happyShift action_48
action_27 (39) = happyShift action_49
action_27 (40) = happyShift action_50
action_27 (41) = happyShift action_51
action_27 (42) = happyShift action_52
action_27 (43) = happyShift action_53
action_27 (44) = happyShift action_54
action_27 (45) = happyShift action_55
action_27 (6) = happyGoto action_39
action_27 _ = happyReduce_24

action_28 (22) = happyShift action_40
action_28 (31) = happyShift action_41
action_28 (32) = happyShift action_42
action_28 (33) = happyShift action_43
action_28 (34) = happyShift action_44
action_28 (35) = happyShift action_45
action_28 (36) = happyShift action_46
action_28 (37) = happyShift action_47
action_28 (38) = happyShift action_48
action_28 (39) = happyShift action_49
action_28 (40) = happyShift action_50
action_28 (41) = happyShift action_51
action_28 (42) = happyShift action_52
action_28 (43) = happyShift action_53
action_28 (44) = happyShift action_54
action_28 (45) = happyShift action_55
action_28 (48) = happyShift action_69
action_28 (50) = happyShift action_56
action_28 (52) = happyShift action_57
action_28 (6) = happyGoto action_39
action_28 _ = happyFail

action_29 (22) = happyShift action_40
action_29 (31) = happyShift action_41
action_29 (32) = happyShift action_42
action_29 (33) = happyShift action_43
action_29 (34) = happyShift action_44
action_29 (35) = happyShift action_45
action_29 (36) = happyShift action_46
action_29 (37) = happyShift action_47
action_29 (38) = happyShift action_48
action_29 (39) = happyShift action_49
action_29 (40) = happyShift action_50
action_29 (41) = happyShift action_51
action_29 (42) = happyShift action_52
action_29 (43) = happyShift action_53
action_29 (44) = happyShift action_54
action_29 (45) = happyShift action_55
action_29 (6) = happyGoto action_39
action_29 _ = happyReduce_23

action_30 (22) = happyShift action_40
action_30 (6) = happyGoto action_39
action_30 _ = happyReduce_14

action_31 (44) = happyShift action_68
action_31 _ = happyFail

action_32 (22) = happyShift action_40
action_32 (27) = happyShift action_67
action_32 (31) = happyShift action_41
action_32 (32) = happyShift action_42
action_32 (33) = happyShift action_43
action_32 (34) = happyShift action_44
action_32 (35) = happyShift action_45
action_32 (36) = happyShift action_46
action_32 (37) = happyShift action_47
action_32 (38) = happyShift action_48
action_32 (39) = happyShift action_49
action_32 (40) = happyShift action_50
action_32 (41) = happyShift action_51
action_32 (42) = happyShift action_52
action_32 (43) = happyShift action_53
action_32 (44) = happyShift action_54
action_32 (45) = happyShift action_55
action_32 (50) = happyShift action_56
action_32 (52) = happyShift action_57
action_32 (6) = happyGoto action_39
action_32 _ = happyFail

action_33 (25) = happyShift action_66
action_33 _ = happyFail

action_34 (22) = happyShift action_40
action_34 (23) = happyShift action_65
action_34 (31) = happyShift action_41
action_34 (32) = happyShift action_42
action_34 (33) = happyShift action_43
action_34 (34) = happyShift action_44
action_34 (35) = happyShift action_45
action_34 (36) = happyShift action_46
action_34 (37) = happyShift action_47
action_34 (38) = happyShift action_48
action_34 (39) = happyShift action_49
action_34 (40) = happyShift action_50
action_34 (41) = happyShift action_51
action_34 (42) = happyShift action_52
action_34 (43) = happyShift action_53
action_34 (44) = happyShift action_54
action_34 (45) = happyShift action_55
action_34 (50) = happyShift action_56
action_34 (52) = happyShift action_57
action_34 (6) = happyGoto action_39
action_34 _ = happyFail

action_35 (22) = happyShift action_40
action_35 (31) = happyShift action_41
action_35 (32) = happyShift action_42
action_35 (33) = happyShift action_43
action_35 (34) = happyShift action_44
action_35 (35) = happyShift action_45
action_35 (36) = happyShift action_46
action_35 (37) = happyShift action_47
action_35 (40) = happyShift action_50
action_35 (41) = happyShift action_51
action_35 (42) = happyShift action_52
action_35 (43) = happyShift action_53
action_35 (44) = happyShift action_54
action_35 (45) = happyShift action_55
action_35 (6) = happyGoto action_39
action_35 _ = happyReduce_15

action_36 (9) = happyShift action_3
action_36 (10) = happyShift action_4
action_36 (11) = happyShift action_5
action_36 (12) = happyShift action_6
action_36 (13) = happyShift action_7
action_36 (14) = happyShift action_8
action_36 (15) = happyShift action_9
action_36 (16) = happyShift action_10
action_36 (19) = happyShift action_11
action_36 (20) = happyShift action_12
action_36 (21) = happyShift action_13
action_36 (22) = happyShift action_14
action_36 (24) = happyShift action_15
action_36 (26) = happyShift action_16
action_36 (29) = happyShift action_17
action_36 (32) = happyShift action_18
action_36 (46) = happyShift action_19
action_36 (47) = happyShift action_20
action_36 (51) = happyShift action_21
action_36 (5) = happyGoto action_64
action_36 _ = happyFail

action_37 (9) = happyShift action_3
action_37 (10) = happyShift action_4
action_37 (11) = happyShift action_5
action_37 (12) = happyShift action_6
action_37 (13) = happyShift action_7
action_37 (14) = happyShift action_8
action_37 (15) = happyShift action_9
action_37 (16) = happyShift action_10
action_37 (19) = happyShift action_11
action_37 (20) = happyShift action_12
action_37 (21) = happyShift action_13
action_37 (22) = happyShift action_14
action_37 (24) = happyShift action_15
action_37 (26) = happyShift action_16
action_37 (29) = happyShift action_17
action_37 (32) = happyShift action_18
action_37 (46) = happyShift action_19
action_37 (47) = happyShift action_20
action_37 (51) = happyShift action_21
action_37 (5) = happyGoto action_63
action_37 _ = happyFail

action_38 (17) = happyShift action_62
action_38 (22) = happyShift action_40
action_38 (31) = happyShift action_41
action_38 (32) = happyShift action_42
action_38 (33) = happyShift action_43
action_38 (34) = happyShift action_44
action_38 (35) = happyShift action_45
action_38 (36) = happyShift action_46
action_38 (37) = happyShift action_47
action_38 (38) = happyShift action_48
action_38 (39) = happyShift action_49
action_38 (40) = happyShift action_50
action_38 (41) = happyShift action_51
action_38 (42) = happyShift action_52
action_38 (43) = happyShift action_53
action_38 (44) = happyShift action_54
action_38 (45) = happyShift action_55
action_38 (50) = happyShift action_56
action_38 (52) = happyShift action_57
action_38 (6) = happyGoto action_39
action_38 _ = happyFail

action_39 (9) = happyShift action_3
action_39 (10) = happyShift action_4
action_39 (11) = happyShift action_5
action_39 (12) = happyShift action_6
action_39 (13) = happyShift action_7
action_39 (14) = happyShift action_8
action_39 (15) = happyShift action_9
action_39 (16) = happyShift action_10
action_39 (19) = happyShift action_11
action_39 (20) = happyShift action_12
action_39 (21) = happyShift action_13
action_39 (22) = happyShift action_14
action_39 (24) = happyShift action_15
action_39 (26) = happyShift action_16
action_39 (29) = happyShift action_17
action_39 (32) = happyShift action_18
action_39 (46) = happyShift action_19
action_39 (47) = happyShift action_20
action_39 (51) = happyShift action_21
action_39 (5) = happyGoto action_61
action_39 _ = happyFail

action_40 (9) = happyShift action_3
action_40 (10) = happyShift action_4
action_40 (11) = happyShift action_5
action_40 (12) = happyShift action_6
action_40 (13) = happyShift action_7
action_40 (14) = happyShift action_8
action_40 (15) = happyShift action_9
action_40 (16) = happyShift action_10
action_40 (19) = happyShift action_11
action_40 (20) = happyShift action_12
action_40 (21) = happyShift action_13
action_40 (22) = happyShift action_14
action_40 (24) = happyShift action_15
action_40 (26) = happyShift action_16
action_40 (29) = happyShift action_17
action_40 (32) = happyShift action_18
action_40 (46) = happyShift action_19
action_40 (47) = happyShift action_20
action_40 (51) = happyShift action_21
action_40 (5) = happyGoto action_60
action_40 _ = happyFail

action_41 _ = happyReduce_27

action_42 _ = happyReduce_28

action_43 _ = happyReduce_29

action_44 _ = happyReduce_30

action_45 _ = happyReduce_31

action_46 _ = happyReduce_32

action_47 _ = happyReduce_33

action_48 _ = happyReduce_34

action_49 _ = happyReduce_35

action_50 _ = happyReduce_36

action_51 _ = happyReduce_37

action_52 _ = happyReduce_38

action_53 _ = happyReduce_39

action_54 _ = happyReduce_40

action_55 _ = happyReduce_41

action_56 (9) = happyShift action_3
action_56 (10) = happyShift action_4
action_56 (11) = happyShift action_5
action_56 (12) = happyShift action_6
action_56 (13) = happyShift action_7
action_56 (14) = happyShift action_8
action_56 (15) = happyShift action_9
action_56 (16) = happyShift action_10
action_56 (19) = happyShift action_11
action_56 (20) = happyShift action_12
action_56 (21) = happyShift action_13
action_56 (22) = happyShift action_14
action_56 (24) = happyShift action_15
action_56 (26) = happyShift action_16
action_56 (29) = happyShift action_17
action_56 (32) = happyShift action_18
action_56 (46) = happyShift action_19
action_56 (47) = happyShift action_20
action_56 (51) = happyShift action_21
action_56 (5) = happyGoto action_59
action_56 _ = happyFail

action_57 (9) = happyShift action_3
action_57 (10) = happyShift action_4
action_57 (11) = happyShift action_5
action_57 (12) = happyShift action_6
action_57 (13) = happyShift action_7
action_57 (14) = happyShift action_8
action_57 (15) = happyShift action_9
action_57 (16) = happyShift action_10
action_57 (19) = happyShift action_11
action_57 (20) = happyShift action_12
action_57 (21) = happyShift action_13
action_57 (22) = happyShift action_14
action_57 (24) = happyShift action_15
action_57 (26) = happyShift action_16
action_57 (29) = happyShift action_17
action_57 (32) = happyShift action_18
action_57 (46) = happyShift action_19
action_57 (47) = happyShift action_20
action_57 (51) = happyShift action_21
action_57 (5) = happyGoto action_58
action_57 _ = happyFail

action_58 (22) = happyShift action_40
action_58 (31) = happyShift action_41
action_58 (32) = happyShift action_42
action_58 (33) = happyShift action_43
action_58 (34) = happyShift action_44
action_58 (35) = happyShift action_45
action_58 (36) = happyShift action_46
action_58 (37) = happyShift action_47
action_58 (38) = happyShift action_48
action_58 (39) = happyShift action_49
action_58 (40) = happyShift action_50
action_58 (41) = happyShift action_51
action_58 (42) = happyShift action_52
action_58 (43) = happyShift action_53
action_58 (44) = happyShift action_54
action_58 (45) = happyShift action_55
action_58 (50) = happyShift action_56
action_58 (52) = happyFail
action_58 (6) = happyGoto action_39
action_58 _ = happyReduce_22

action_59 (22) = happyShift action_40
action_59 (31) = happyShift action_41
action_59 (32) = happyShift action_42
action_59 (33) = happyShift action_43
action_59 (34) = happyShift action_44
action_59 (35) = happyShift action_45
action_59 (36) = happyShift action_46
action_59 (37) = happyShift action_47
action_59 (38) = happyShift action_48
action_59 (39) = happyShift action_49
action_59 (40) = happyShift action_50
action_59 (41) = happyShift action_51
action_59 (42) = happyShift action_52
action_59 (43) = happyShift action_53
action_59 (44) = happyShift action_54
action_59 (45) = happyShift action_55
action_59 (6) = happyGoto action_39
action_59 _ = happyReduce_25

action_60 (22) = happyShift action_40
action_60 (23) = happyShift action_81
action_60 (31) = happyShift action_41
action_60 (32) = happyShift action_42
action_60 (33) = happyShift action_43
action_60 (34) = happyShift action_44
action_60 (35) = happyShift action_45
action_60 (36) = happyShift action_46
action_60 (37) = happyShift action_47
action_60 (38) = happyShift action_48
action_60 (39) = happyShift action_49
action_60 (40) = happyShift action_50
action_60 (41) = happyShift action_51
action_60 (42) = happyShift action_52
action_60 (43) = happyShift action_53
action_60 (44) = happyShift action_54
action_60 (45) = happyShift action_55
action_60 (50) = happyShift action_56
action_60 (52) = happyShift action_57
action_60 (6) = happyGoto action_39
action_60 _ = happyFail

action_61 (22) = happyShift action_40
action_61 (31) = happyShift action_41
action_61 (32) = happyShift action_42
action_61 (33) = happyShift action_43
action_61 (34) = happyShift action_44
action_61 (35) = happyShift action_45
action_61 (36) = happyShift action_46
action_61 (37) = happyShift action_47
action_61 (38) = happyShift action_48
action_61 (39) = happyShift action_49
action_61 (40) = happyShift action_50
action_61 (41) = happyShift action_51
action_61 (42) = happyShift action_52
action_61 (43) = happyShift action_53
action_61 (44) = happyShift action_54
action_61 (45) = happyShift action_55
action_61 (6) = happyGoto action_39
action_61 _ = happyReduce_19

action_62 (9) = happyShift action_3
action_62 (10) = happyShift action_4
action_62 (11) = happyShift action_5
action_62 (12) = happyShift action_6
action_62 (13) = happyShift action_7
action_62 (14) = happyShift action_8
action_62 (15) = happyShift action_9
action_62 (16) = happyShift action_10
action_62 (19) = happyShift action_11
action_62 (20) = happyShift action_12
action_62 (21) = happyShift action_13
action_62 (22) = happyShift action_14
action_62 (24) = happyShift action_15
action_62 (26) = happyShift action_16
action_62 (29) = happyShift action_17
action_62 (32) = happyShift action_18
action_62 (46) = happyShift action_19
action_62 (47) = happyShift action_20
action_62 (51) = happyShift action_21
action_62 (5) = happyGoto action_80
action_62 _ = happyFail

action_63 (22) = happyShift action_40
action_63 (23) = happyShift action_79
action_63 (31) = happyShift action_41
action_63 (32) = happyShift action_42
action_63 (33) = happyShift action_43
action_63 (34) = happyShift action_44
action_63 (35) = happyShift action_45
action_63 (36) = happyShift action_46
action_63 (37) = happyShift action_47
action_63 (38) = happyShift action_48
action_63 (39) = happyShift action_49
action_63 (40) = happyShift action_50
action_63 (41) = happyShift action_51
action_63 (42) = happyShift action_52
action_63 (43) = happyShift action_53
action_63 (44) = happyShift action_54
action_63 (45) = happyShift action_55
action_63 (50) = happyShift action_56
action_63 (52) = happyShift action_57
action_63 (6) = happyGoto action_39
action_63 _ = happyFail

action_64 (22) = happyShift action_40
action_64 (23) = happyShift action_78
action_64 (31) = happyShift action_41
action_64 (32) = happyShift action_42
action_64 (33) = happyShift action_43
action_64 (34) = happyShift action_44
action_64 (35) = happyShift action_45
action_64 (36) = happyShift action_46
action_64 (37) = happyShift action_47
action_64 (38) = happyShift action_48
action_64 (39) = happyShift action_49
action_64 (40) = happyShift action_50
action_64 (41) = happyShift action_51
action_64 (42) = happyShift action_52
action_64 (43) = happyShift action_53
action_64 (44) = happyShift action_54
action_64 (45) = happyShift action_55
action_64 (50) = happyShift action_56
action_64 (52) = happyShift action_57
action_64 (6) = happyGoto action_39
action_64 _ = happyFail

action_65 _ = happyReduce_16

action_66 (9) = happyShift action_3
action_66 (10) = happyShift action_4
action_66 (11) = happyShift action_5
action_66 (12) = happyShift action_6
action_66 (13) = happyShift action_7
action_66 (14) = happyShift action_8
action_66 (15) = happyShift action_9
action_66 (16) = happyShift action_10
action_66 (19) = happyShift action_11
action_66 (20) = happyShift action_12
action_66 (21) = happyShift action_13
action_66 (22) = happyShift action_14
action_66 (24) = happyShift action_15
action_66 (26) = happyShift action_16
action_66 (29) = happyShift action_17
action_66 (32) = happyShift action_18
action_66 (46) = happyShift action_19
action_66 (47) = happyShift action_20
action_66 (51) = happyShift action_21
action_66 (5) = happyGoto action_77
action_66 _ = happyFail

action_67 (9) = happyShift action_3
action_67 (10) = happyShift action_4
action_67 (11) = happyShift action_5
action_67 (12) = happyShift action_6
action_67 (13) = happyShift action_7
action_67 (14) = happyShift action_8
action_67 (15) = happyShift action_9
action_67 (16) = happyShift action_10
action_67 (19) = happyShift action_11
action_67 (20) = happyShift action_12
action_67 (21) = happyShift action_13
action_67 (22) = happyShift action_14
action_67 (24) = happyShift action_15
action_67 (26) = happyShift action_16
action_67 (29) = happyShift action_17
action_67 (32) = happyShift action_18
action_67 (46) = happyShift action_19
action_67 (47) = happyShift action_20
action_67 (51) = happyShift action_21
action_67 (5) = happyGoto action_76
action_67 _ = happyFail

action_68 (9) = happyShift action_3
action_68 (10) = happyShift action_4
action_68 (11) = happyShift action_5
action_68 (12) = happyShift action_6
action_68 (13) = happyShift action_7
action_68 (14) = happyShift action_8
action_68 (15) = happyShift action_9
action_68 (16) = happyShift action_10
action_68 (19) = happyShift action_11
action_68 (20) = happyShift action_12
action_68 (21) = happyShift action_13
action_68 (22) = happyShift action_14
action_68 (24) = happyShift action_15
action_68 (26) = happyShift action_16
action_68 (29) = happyShift action_17
action_68 (32) = happyShift action_18
action_68 (46) = happyShift action_19
action_68 (47) = happyShift action_20
action_68 (51) = happyShift action_21
action_68 (5) = happyGoto action_75
action_68 _ = happyFail

action_69 (9) = happyShift action_3
action_69 (10) = happyShift action_4
action_69 (11) = happyShift action_5
action_69 (12) = happyShift action_6
action_69 (13) = happyShift action_7
action_69 (14) = happyShift action_8
action_69 (15) = happyShift action_9
action_69 (16) = happyShift action_10
action_69 (19) = happyShift action_11
action_69 (20) = happyShift action_12
action_69 (21) = happyShift action_13
action_69 (22) = happyShift action_14
action_69 (24) = happyShift action_15
action_69 (26) = happyShift action_16
action_69 (29) = happyShift action_17
action_69 (32) = happyShift action_18
action_69 (46) = happyShift action_19
action_69 (47) = happyShift action_20
action_69 (51) = happyShift action_21
action_69 (5) = happyGoto action_74
action_69 _ = happyFail

action_70 (9) = happyShift action_3
action_70 (10) = happyShift action_4
action_70 (11) = happyShift action_5
action_70 (12) = happyShift action_6
action_70 (13) = happyShift action_7
action_70 (14) = happyShift action_8
action_70 (15) = happyShift action_9
action_70 (16) = happyShift action_10
action_70 (19) = happyShift action_11
action_70 (20) = happyShift action_12
action_70 (21) = happyShift action_13
action_70 (22) = happyShift action_14
action_70 (24) = happyShift action_15
action_70 (26) = happyShift action_16
action_70 (29) = happyShift action_17
action_70 (32) = happyShift action_18
action_70 (46) = happyShift action_19
action_70 (47) = happyShift action_20
action_70 (51) = happyShift action_21
action_70 (5) = happyGoto action_73
action_70 _ = happyFail

action_71 (9) = happyShift action_3
action_71 (10) = happyShift action_4
action_71 (11) = happyShift action_5
action_71 (12) = happyShift action_6
action_71 (13) = happyShift action_7
action_71 (14) = happyShift action_8
action_71 (15) = happyShift action_9
action_71 (16) = happyShift action_10
action_71 (19) = happyShift action_11
action_71 (20) = happyShift action_12
action_71 (21) = happyShift action_13
action_71 (22) = happyShift action_14
action_71 (24) = happyShift action_15
action_71 (26) = happyShift action_16
action_71 (29) = happyShift action_17
action_71 (32) = happyShift action_18
action_71 (46) = happyShift action_19
action_71 (47) = happyShift action_20
action_71 (51) = happyShift action_21
action_71 (5) = happyGoto action_72
action_71 _ = happyFail

action_72 (22) = happyShift action_40
action_72 (31) = happyShift action_41
action_72 (32) = happyShift action_42
action_72 (33) = happyShift action_43
action_72 (34) = happyShift action_44
action_72 (35) = happyShift action_45
action_72 (36) = happyShift action_46
action_72 (37) = happyShift action_47
action_72 (38) = happyShift action_48
action_72 (39) = happyShift action_49
action_72 (40) = happyShift action_50
action_72 (41) = happyShift action_51
action_72 (42) = happyShift action_52
action_72 (43) = happyShift action_53
action_72 (44) = happyShift action_54
action_72 (45) = happyShift action_55
action_72 (50) = happyShift action_56
action_72 (52) = happyShift action_57
action_72 (6) = happyGoto action_39
action_72 _ = happyReduce_3

action_73 (22) = happyShift action_40
action_73 (31) = happyShift action_41
action_73 (32) = happyShift action_42
action_73 (33) = happyShift action_43
action_73 (34) = happyShift action_44
action_73 (35) = happyShift action_45
action_73 (36) = happyShift action_46
action_73 (37) = happyShift action_47
action_73 (38) = happyShift action_48
action_73 (39) = happyShift action_49
action_73 (40) = happyShift action_50
action_73 (41) = happyShift action_51
action_73 (42) = happyShift action_52
action_73 (43) = happyShift action_53
action_73 (44) = happyShift action_54
action_73 (45) = happyShift action_55
action_73 (50) = happyShift action_56
action_73 (52) = happyShift action_57
action_73 (6) = happyGoto action_39
action_73 _ = happyReduce_2

action_74 (22) = happyShift action_40
action_74 (31) = happyShift action_41
action_74 (32) = happyShift action_42
action_74 (33) = happyShift action_43
action_74 (34) = happyShift action_44
action_74 (35) = happyShift action_45
action_74 (36) = happyShift action_46
action_74 (37) = happyShift action_47
action_74 (38) = happyShift action_48
action_74 (39) = happyShift action_49
action_74 (40) = happyShift action_50
action_74 (41) = happyShift action_51
action_74 (42) = happyShift action_52
action_74 (43) = happyShift action_53
action_74 (44) = happyShift action_54
action_74 (45) = happyShift action_55
action_74 (49) = happyShift action_85
action_74 (50) = happyShift action_56
action_74 (52) = happyShift action_57
action_74 (6) = happyGoto action_39
action_74 _ = happyFail

action_75 (22) = happyShift action_40
action_75 (30) = happyShift action_84
action_75 (31) = happyShift action_41
action_75 (32) = happyShift action_42
action_75 (33) = happyShift action_43
action_75 (34) = happyShift action_44
action_75 (35) = happyShift action_45
action_75 (36) = happyShift action_46
action_75 (37) = happyShift action_47
action_75 (38) = happyShift action_48
action_75 (39) = happyShift action_49
action_75 (40) = happyShift action_50
action_75 (41) = happyShift action_51
action_75 (42) = happyShift action_52
action_75 (43) = happyShift action_53
action_75 (44) = happyShift action_54
action_75 (45) = happyShift action_55
action_75 (50) = happyShift action_56
action_75 (52) = happyShift action_57
action_75 (6) = happyGoto action_39
action_75 _ = happyFail

action_76 (22) = happyShift action_40
action_76 (28) = happyShift action_83
action_76 (31) = happyShift action_41
action_76 (32) = happyShift action_42
action_76 (33) = happyShift action_43
action_76 (34) = happyShift action_44
action_76 (35) = happyShift action_45
action_76 (36) = happyShift action_46
action_76 (37) = happyShift action_47
action_76 (38) = happyShift action_48
action_76 (39) = happyShift action_49
action_76 (40) = happyShift action_50
action_76 (41) = happyShift action_51
action_76 (42) = happyShift action_52
action_76 (43) = happyShift action_53
action_76 (44) = happyShift action_54
action_76 (45) = happyShift action_55
action_76 (50) = happyShift action_56
action_76 (52) = happyShift action_57
action_76 (6) = happyGoto action_39
action_76 _ = happyFail

action_77 (22) = happyShift action_40
action_77 (31) = happyShift action_41
action_77 (32) = happyShift action_42
action_77 (33) = happyShift action_43
action_77 (34) = happyShift action_44
action_77 (35) = happyShift action_45
action_77 (36) = happyShift action_46
action_77 (37) = happyShift action_47
action_77 (38) = happyShift action_48
action_77 (39) = happyShift action_49
action_77 (40) = happyShift action_50
action_77 (41) = happyShift action_51
action_77 (42) = happyShift action_52
action_77 (43) = happyShift action_53
action_77 (44) = happyShift action_54
action_77 (45) = happyShift action_55
action_77 (6) = happyGoto action_39
action_77 _ = happyReduce_17

action_78 _ = happyReduce_13

action_79 _ = happyReduce_12

action_80 (18) = happyShift action_82
action_80 (22) = happyShift action_40
action_80 (31) = happyShift action_41
action_80 (32) = happyShift action_42
action_80 (33) = happyShift action_43
action_80 (34) = happyShift action_44
action_80 (35) = happyShift action_45
action_80 (36) = happyShift action_46
action_80 (37) = happyShift action_47
action_80 (38) = happyShift action_48
action_80 (39) = happyShift action_49
action_80 (40) = happyShift action_50
action_80 (41) = happyShift action_51
action_80 (42) = happyShift action_52
action_80 (43) = happyShift action_53
action_80 (44) = happyShift action_54
action_80 (45) = happyShift action_55
action_80 (50) = happyShift action_56
action_80 (52) = happyShift action_57
action_80 (6) = happyGoto action_39
action_80 _ = happyFail

action_81 _ = happyReduce_18

action_82 _ = happyReduce_11

action_83 (9) = happyShift action_3
action_83 (10) = happyShift action_4
action_83 (11) = happyShift action_5
action_83 (12) = happyShift action_6
action_83 (13) = happyShift action_7
action_83 (14) = happyShift action_8
action_83 (15) = happyShift action_9
action_83 (16) = happyShift action_10
action_83 (19) = happyShift action_11
action_83 (20) = happyShift action_12
action_83 (21) = happyShift action_13
action_83 (22) = happyShift action_14
action_83 (24) = happyShift action_15
action_83 (26) = happyShift action_16
action_83 (29) = happyShift action_17
action_83 (32) = happyShift action_18
action_83 (46) = happyShift action_19
action_83 (47) = happyShift action_20
action_83 (51) = happyShift action_21
action_83 (5) = happyGoto action_87
action_83 _ = happyFail

action_84 (9) = happyShift action_3
action_84 (10) = happyShift action_4
action_84 (11) = happyShift action_5
action_84 (12) = happyShift action_6
action_84 (13) = happyShift action_7
action_84 (14) = happyShift action_8
action_84 (15) = happyShift action_9
action_84 (16) = happyShift action_10
action_84 (19) = happyShift action_11
action_84 (20) = happyShift action_12
action_84 (21) = happyShift action_13
action_84 (22) = happyShift action_14
action_84 (24) = happyShift action_15
action_84 (26) = happyShift action_16
action_84 (29) = happyShift action_17
action_84 (32) = happyShift action_18
action_84 (46) = happyShift action_19
action_84 (47) = happyShift action_20
action_84 (51) = happyShift action_21
action_84 (5) = happyGoto action_86
action_84 _ = happyFail

action_85 _ = happyReduce_26

action_86 (22) = happyShift action_40
action_86 (31) = happyShift action_41
action_86 (32) = happyShift action_42
action_86 (33) = happyShift action_43
action_86 (34) = happyShift action_44
action_86 (35) = happyShift action_45
action_86 (36) = happyShift action_46
action_86 (37) = happyShift action_47
action_86 (38) = happyShift action_48
action_86 (39) = happyShift action_49
action_86 (40) = happyShift action_50
action_86 (41) = happyShift action_51
action_86 (42) = happyShift action_52
action_86 (43) = happyShift action_53
action_86 (44) = happyShift action_54
action_86 (45) = happyShift action_55
action_86 (6) = happyGoto action_39
action_86 _ = happyReduce_21

action_87 (22) = happyShift action_40
action_87 (31) = happyShift action_41
action_87 (32) = happyShift action_42
action_87 (33) = happyShift action_43
action_87 (34) = happyShift action_44
action_87 (35) = happyShift action_45
action_87 (36) = happyShift action_46
action_87 (37) = happyShift action_47
action_87 (38) = happyShift action_48
action_87 (39) = happyShift action_49
action_87 (40) = happyShift action_50
action_87 (41) = happyShift action_51
action_87 (42) = happyShift action_52
action_87 (43) = happyShift action_53
action_87 (44) = happyShift action_54
action_87 (45) = happyShift action_55
action_87 (6) = happyGoto action_39
action_87 _ = happyReduce_20

happyReduce_1 = happySpecReduce_1  4 happyReduction_1
happyReduction_1 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn4
		 (ExpressionStmt happy_var_1
	)
happyReduction_1 _  = notHappyAtAll 

happyReduce_2 = happyReduce 4 4 happyReduction_2
happyReduction_2 ((HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (VarT happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn4
		 (GlobalVarStmt happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_3 = happyReduce 4 4 happyReduction_3
happyReduction_3 ((HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (VarT happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn4
		 (RecurFunctStmt happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_4 = happySpecReduce_1  5 happyReduction_4
happyReduction_4 (HappyTerminal (IntT happy_var_1))
	 =  HappyAbsSyn5
		 (IntExp happy_var_1
	)
happyReduction_4 _  = notHappyAtAll 

happyReduce_5 = happySpecReduce_1  5 happyReduction_5
happyReduction_5 (HappyTerminal (FloatT happy_var_1))
	 =  HappyAbsSyn5
		 (RealExp happy_var_1
	)
happyReduction_5 _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1  5 happyReduction_6
happyReduction_6 _
	 =  HappyAbsSyn5
		 (BoolExp True
	)

happyReduce_7 = happySpecReduce_1  5 happyReduction_7
happyReduction_7 _
	 =  HappyAbsSyn5
		 (BoolExp False
	)

happyReduce_8 = happySpecReduce_1  5 happyReduction_8
happyReduction_8 (HappyTerminal (StringT happy_var_1))
	 =  HappyAbsSyn5
		 (StringExp happy_var_1
	)
happyReduction_8 _  = notHappyAtAll 

happyReduce_9 = happySpecReduce_1  5 happyReduction_9
happyReduction_9 (HappyTerminal (VarT happy_var_1))
	 =  HappyAbsSyn5
		 (VarExp happy_var_1
	)
happyReduction_9 _  = notHappyAtAll 

happyReduce_10 = happySpecReduce_1  5 happyReduction_10
happyReduction_10 _
	 =  HappyAbsSyn5
		 (UnitExp
	)

happyReduce_11 = happyReduce 5 5 happyReduction_11
happyReduction_11 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (PairExp happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_12 = happyReduce 4 5 happyReduction_12
happyReduction_12 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (FstExp happy_var_3
	) `HappyStk` happyRest

happyReduce_13 = happyReduce 4 5 happyReduction_13
happyReduction_13 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (SndExp happy_var_3
	) `HappyStk` happyRest

happyReduce_14 = happySpecReduce_2  5 happyReduction_14
happyReduction_14 (HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn5
		 (NegateExp happy_var_2
	)
happyReduction_14 _ _  = notHappyAtAll 

happyReduce_15 = happySpecReduce_2  5 happyReduction_15
happyReduction_15 (HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn5
		 (NegateExp happy_var_2
	)
happyReduction_15 _ _  = notHappyAtAll 

happyReduce_16 = happySpecReduce_3  5 happyReduction_16
happyReduction_16 _
	(HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn5
		 (happy_var_2
	)
happyReduction_16 _ _ _  = notHappyAtAll 

happyReduce_17 = happyReduce 4 5 happyReduction_17
happyReduction_17 ((HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (VarT happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (FunctExp happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_18 = happyReduce 4 5 happyReduction_18
happyReduction_18 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (ApplicationExp happy_var_1 happy_var_3
	) `HappyStk` happyRest

happyReduce_19 = happySpecReduce_3  5 happyReduction_19
happyReduction_19 (HappyAbsSyn5  happy_var_3)
	(HappyAbsSyn6  happy_var_2)
	(HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn5
		 (BinOpExp happy_var_1 happy_var_2 happy_var_3
	)
happyReduction_19 _ _ _  = notHappyAtAll 

happyReduce_20 = happyReduce 6 5 happyReduction_20
happyReduction_20 ((HappyAbsSyn5  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (IfThenElseExp happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_21 = happyReduce 6 5 happyReduction_21
happyReduction_21 ((HappyAbsSyn5  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (VarT happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (LocalVarExp happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_22 = happySpecReduce_3  5 happyReduction_22
happyReduction_22 (HappyAbsSyn5  happy_var_3)
	_
	(HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn5
		 (SeqExp happy_var_1 happy_var_3
	)
happyReduction_22 _ _ _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_2  5 happyReduction_23
happyReduction_23 (HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn5
		 (RefExp happy_var_2
	)
happyReduction_23 _ _  = notHappyAtAll 

happyReduce_24 = happySpecReduce_2  5 happyReduction_24
happyReduction_24 (HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn5
		 (DeRefExp happy_var_2
	)
happyReduction_24 _ _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_3  5 happyReduction_25
happyReduction_25 (HappyAbsSyn5  happy_var_3)
	_
	(HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn5
		 (AssignExp happy_var_1 happy_var_3
	)
happyReduction_25 _ _ _  = notHappyAtAll 

happyReduce_26 = happyReduce 5 5 happyReduction_26
happyReduction_26 (_ `HappyStk`
	(HappyAbsSyn5  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn5  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (WhileExp happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_27 = happySpecReduce_1  6 happyReduction_27
happyReduction_27 _
	 =  HappyAbsSyn6
		 (PlusOp
	)

happyReduce_28 = happySpecReduce_1  6 happyReduction_28
happyReduction_28 _
	 =  HappyAbsSyn6
		 (MinusOp
	)

happyReduce_29 = happySpecReduce_1  6 happyReduction_29
happyReduction_29 _
	 =  HappyAbsSyn6
		 (MultOp
	)

happyReduce_30 = happySpecReduce_1  6 happyReduction_30
happyReduction_30 _
	 =  HappyAbsSyn6
		 (DivOp
	)

happyReduce_31 = happySpecReduce_1  6 happyReduction_31
happyReduction_31 _
	 =  HappyAbsSyn6
		 (IntDivOp
	)

happyReduce_32 = happySpecReduce_1  6 happyReduction_32
happyReduction_32 _
	 =  HappyAbsSyn6
		 (ModOp
	)

happyReduce_33 = happySpecReduce_1  6 happyReduction_33
happyReduction_33 _
	 =  HappyAbsSyn6
		 (ExponentOp
	)

happyReduce_34 = happySpecReduce_1  6 happyReduction_34
happyReduction_34 _
	 =  HappyAbsSyn6
		 (AndOp
	)

happyReduce_35 = happySpecReduce_1  6 happyReduction_35
happyReduction_35 _
	 =  HappyAbsSyn6
		 (OrOp
	)

happyReduce_36 = happySpecReduce_1  6 happyReduction_36
happyReduction_36 _
	 =  HappyAbsSyn6
		 (LtOp
	)

happyReduce_37 = happySpecReduce_1  6 happyReduction_37
happyReduction_37 _
	 =  HappyAbsSyn6
		 (LtEqOp
	)

happyReduce_38 = happySpecReduce_1  6 happyReduction_38
happyReduction_38 _
	 =  HappyAbsSyn6
		 (GtOp
	)

happyReduce_39 = happySpecReduce_1  6 happyReduction_39
happyReduction_39 _
	 =  HappyAbsSyn6
		 (GtEqOp
	)

happyReduce_40 = happySpecReduce_1  6 happyReduction_40
happyReduction_40 _
	 =  HappyAbsSyn6
		 (EqOp
	)

happyReduce_41 = happySpecReduce_1  6 happyReduction_41
happyReduction_41 _
	 =  HappyAbsSyn6
		 (NEqOp
	)

happyNewToken action sts stk [] =
	action 53 53 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	AliveT -> cont 7;
	UndedT -> cont 8;
	FloatT happy_dollar_dollar -> cont 9;
	IntT happy_dollar_dollar -> cont 10;
	TrueT -> cont 11;
	FalseT -> cont 12;
	StringT happy_dollar_dollar -> cont 13;
	VarT happy_dollar_dollar -> cont 14;
	UnitT -> cont 15;
	LBracketT -> cont 16;
	CommaT -> cont 17;
	RBracketT -> cont 18;
	FstT -> cont 19;
	SndT -> cont 20;
	NotT -> cont 21;
	LParenT -> cont 22;
	RParenT -> cont 23;
	FunctT -> cont 24;
	ArrowT -> cont 25;
	IfT -> cont 26;
	ThenT -> cont 27;
	ElseT -> cont 28;
	IveT -> cont 29;
	InT -> cont 30;
	PlusT -> cont 31;
	MinusT -> cont 32;
	MultT -> cont 33;
	DivT -> cont 34;
	IntDivT -> cont 35;
	ModT -> cont 36;
	ExponentT -> cont 37;
	AndT -> cont 38;
	OrT -> cont 39;
	LtT -> cont 40;
	LtEqT -> cont 41;
	GtT -> cont 42;
	GtEqT -> cont 43;
	EqT -> cont 44;
	NEqT -> cont 45;
	LitT -> cont 46;
	WhileT -> cont 47;
	DoT -> cont 48;
	DoneT -> cont 49;
	AssignT -> cont 50;
	DeLitT -> cont 51;
	ElipsisT -> cont 52;
	_ -> happyError' (tk:tks)
	}

happyError_ 53 tk tks = happyError' tks
happyError_ _ tk tks = happyError' (tk:tks)

happyThen :: () => Maybe a -> (a -> Maybe b) -> Maybe b
happyThen = (>>=)
happyReturn :: () => a -> Maybe a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> Maybe a
happyReturn1 = \a tks -> (return) a
happyError' :: () => [(Token)] -> Maybe a
happyError' = parseError

parse tks = happySomeParser where
  happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


arithOp :: (Integer -> Integer -> Integer) -> (Float -> Float -> Float) -> Value -> Value -> Value
arithOp intOp floatOp (IntV x) (IntV y) = IntV $ intOp x y
arithOp intOp floatOp (IntV x) (RealV y) = RealV $ floatOp (fromInteger x) y
arithOp intOp floatOp (RealV x) (IntV y) = RealV $ floatOp x (fromInteger y)
arithOp intOp floatOp (RealV x) (RealV y) = RealV $ floatOp  x y
arithOp _ _ _ _ = error "Type mismatch, expected number."

parseError :: [Token] -> Maybe a
parseError _ = Nothing
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
