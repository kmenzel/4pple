module DataTypes where

data Token =
    AliveT          |
    UndedT          |
    IntT Integer    |
    FloatT Float    |
    TrueT           |
    FalseT          |
    StringT String  |
    VarT String     |
    UnitT           |
    LBracketT       |
    CommaT          |
    RBracketT       |
    FstT            |
    SndT            |
    NotT            |
    LParenT         |
    RParenT         |
    FunctT          |
    ArrowT          |
    IfT             |
    ThenT           |
    ElseT           |
    IveT            |
    InT             |
    PlusT           |
    MinusT          |
    MultT           |
    DivT            |
    IntDivT         |
    ModT            |
    ExponentT       |
    AndT            |
    OrT             |
    LtT             |
    LtEqT           |
    GtT             |
    GtEqT           |
    EqT             |
    NEqT            |
    LitT            |
    WhileT          |
    DoT             |
    DoneT           |
    AssignT         |
    DeLitT          |
    ElipsisT deriving (Eq,Show)
--    ConsT           |

type Var = String
type Loc = Integer

type Env = [(Var, Value)]
type Store = [(Loc, Value)]

data Context = Context {env :: Env, store :: Store} deriving (Eq)

instance Show Context where
    show context = "Env: " ++ (show (env context)) ++ "\nStore: " ++ (show (store context)) ++ "\n"


data Value = IntV Integer | RealV Float | BoolV Bool | StringV String | PairV Value Value | FunctV Var Expr Env | LocV Loc | UnitV

instance Eq Value where
    (==) (IntV x) (IntV y) = x == y
    (==) (RealV x) (IntV y) = x == (fromInteger y)
    (==) (IntV x) (RealV y) = (fromInteger x) == y
    (==) (RealV x) (RealV y) = x == y
    (==) (BoolV x) (BoolV y) = x == y
    (==) (StringV x) (StringV y) = x == y
    (==) (PairV x1 x2) (PairV y1 y2) = (x1 == y1) && (x2 == y2)
    (==) (FunctV varName1 varDef1 env1) (FunctV varName2 varDef2 env2) = (varName1 == varName2) && (varDef1 == varDef2) && (env1 == env2)
    (==) (LocV x) (LocV y) = x == y
    (==) UnitV UnitV = True
    (==) _ _ = False

instance Show Value where
    show (IntV x) = show x
    show (RealV x) = show x
    show (BoolV True) = "Yas"
    show (BoolV False) = "Nah"
    show (StringV x) = x
    show (PairV x y) = "[" ++ (show x) ++ "," ++ (show y) ++ "]"
    show (FunctV varName varDef env) = "Function:\n\t" ++ (show varDef)
    show (LocV x) = "Location[" ++ (show x) ++ "]"
    show UnitV = ":D"

instance Ord Value where
    (<=) (IntV x) (IntV y) = x <= y
    (<=) (RealV x) (IntV y) = x <= (fromInteger y)
    (<=) (IntV x) (RealV y) = (fromInteger x) <= y
    (<=) (RealV x) (RealV y) = x <= y
    (<=) _ _ = error "Type mismatch, expected int or real."

data Stmt = ExpressionStmt Expr | GlobalVarStmt Var Expr | RecurFunctStmt Var Expr deriving Show

data Expr = IntExp Integer                  |
            RealExp Float                   |
            BoolExp Bool                    |
            StringExp String                |
            VarExp Var                      |
            UnitExp                         |
            PairExp Expr Expr               |
            FstExp Expr                     |
            SndExp Expr                     |
            FunctExp Var Expr               |
            ApplicationExp Expr Expr        |
            BinOpExp Expr BinOp Expr        |
            NegateExp Expr                  |
            IfThenElseExp Expr Expr Expr    |
            LocalVarExp Var Expr Expr       |
            SeqExp Expr Expr                |
            RefExp Expr                     |
            DeRefExp Expr                   |
            AssignExp Expr Expr             |
            WhileExp Expr Expr deriving (Eq, Show)

data BinOp = PlusOp     |
            MinusOp     |
            MultOp      |
            DivOp       |
            IntDivOp    |
            ModOp       |
            ExponentOp  |
            AndOp       |
            OrOp        |
            LtOp        |
            LtEqOp      |
            GtOp        |
            GtEqOp      |
            EqOp        |
            NEqOp deriving (Eq, Show)
