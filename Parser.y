{
module Parser where
import Lexer
import DataTypes
}

%monad { Maybe } 
%name parse
%tokentype {Token}
%error { parseError}

%token
    alive   { AliveT }
    unded   { UndedT }
    float   { FloatT $$ }
    int     { IntT $$ }
    true    { TrueT }
    false   { FalseT }
    string  { StringT $$ }
    var     { VarT $$ }
    ":D"    { UnitT }
    '['     { LBracketT }
    ','     { CommaT }
    ']'     { RBracketT }
    "1st"   { FstT }
    "2nd"   { SndT }
    not     { NotT }
    '('     { LParenT }
    ')'     { RParenT }
    "fun"   { FunctT }
    "->"    { ArrowT }
    if      { IfT }
    then    { ThenT }
    else    { ElseT }
    ive     { IveT }
    in      { InT }
    '+'     { PlusT } 
    '-'     { MinusT }
    '*'     { MultT }
    '/'     { DivT }
    "//"    { IntDivT }
    '%'     { ModT }
    '^'     { ExponentT }
    and     { AndT }
    or      { OrT }
    '<'     { LtT }
    "<="    { LtEqT }
    '>'     { GtT }
    ">="    { GtEqT }
    '='     { EqT }
    "!="    { NEqT }
    "lit"   { LitT }
    while   { WhileT }
    do      { DoT }
    done    { DoneT }
    "@="    { AssignT }
    "#"     { DeLitT }
    "..."   { ElipsisT }
--  ';'     { ConsT }

--Top of list is least precedent
%nonassoc STMT
%nonassoc "..."
%nonassoc WHILE
%left "@="
%right "lit" "#"
%nonassoc APP
%nonassoc PAIRFUN
%nonassoc IVE
%nonassoc FUN
%left IF
%nonassoc PAIR
%nonassoc BOPPREC
%left and
%left or
%left not
%nonassoc '<' "<=" '>' ">=" '=' "!="
%left '+' '-'
%left '%'
%left '*' '/' "//"
%right '^'
%left NEG
%right '('
--Bottom is most tightly bound

%%
S   : E                             { ExpressionStmt $1 }
    | alive var '=' E %prec STMT    { GlobalVarStmt $2 $4 }
    | unded var '=' E %prec STMT    { RecurFunctStmt $2 $4 }

E   : int                           { IntExp $1 }
    | float                         { RealExp $1 }
    | true                          { BoolExp True }
    | false                         { BoolExp False }
    | string                        { StringExp $1 }
    | var                           { VarExp $1 }
    | ":D"                          { UnitExp }
    | '[' E ',' E ']' %prec PAIR    { PairExp $2 $4 }
    | "1st" '(' E ')'               { FstExp $3 }
    | "2nd" '(' E ')'               { SndExp $3 }
    | '-' E %prec NEG               { NegateExp $2 }
    | not E                         { NegateExp $2 }
    | '(' E ')'                     { $2 }
    | "fun" var "->" E %prec FUN    { FunctExp $2 $4 }
    | E '(' E ')' %prec APP         { ApplicationExp $1 $3 } 
    | E BOp E %prec BOPPREC         { BinOpExp $1 $2 $3 }
    | if E then E else E %prec IF   { IfThenElseExp $2 $4 $6 }
    | ive var '=' E in E %prec IVE  { LocalVarExp $2 $4 $6 }
    | E "..." E                     { SeqExp $1 $3 }
    | "lit" E                       { RefExp $2 }
    | "#" E                         { DeRefExp $2 }
    | E "@=" E                      { AssignExp $1 $3 }
    | while E do E done %prec WHILE { WhileExp $2 $4 }

BOp : '+'                           { PlusOp }
    | '-'                           { MinusOp }
    | '*'                           { MultOp }
    | '/'                           { DivOp }
    | "//"                          { IntDivOp }
    | '%'                           { ModOp }
    |  '^'                          { ExponentOp }
    | and                           { AndOp }
    | or                            { OrOp }
    | '<'                           { LtOp }
    | "<="                          { LtEqOp }
    | '>'                           { GtOp }
    | ">="                          { GtEqOp }
    | '='                           { EqOp }
    | "!="                          { NEqOp }
{

arithOp :: (Integer -> Integer -> Integer) -> (Float -> Float -> Float) -> Value -> Value -> Value
arithOp intOp floatOp (IntV x) (IntV y) = IntV $ intOp x y
arithOp intOp floatOp (IntV x) (RealV y) = RealV $ floatOp (fromInteger x) y
arithOp intOp floatOp (RealV x) (IntV y) = RealV $ floatOp x (fromInteger y)
arithOp intOp floatOp (RealV x) (RealV y) = RealV $ floatOp  x y
arithOp _ _ _ _ = error "Type mismatch, expected number."

parseError :: [Token] -> Maybe a
parseError _ = Nothing

}
