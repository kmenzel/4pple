module Contexts where

import DataTypes

--APPENDING--
--get a fresh location
freshLoc :: Store -> Loc
freshLoc [] = 0
freshLoc store = (maximum (map fst store)) + 1


--add pair to store
addToStore :: (Loc, Value) -> Store -> Store
addToStore val store = val:store

--add pair to env
addToEnv :: (Var, Value) -> Env -> Env
addToEnv val env = val:env


--LOOKUP--
--lookup loc in store
lookupLoc :: Loc -> Store -> Maybe Value
lookupLoc loc [] = Nothing
lookupLoc loc ((iterLoc, value):xs) = if loc == iterLoc 
                                      then (Just value) 
                                      else lookupLoc loc xs

--lookup var in env
lookupVar :: Var -> Env -> Maybe Value
lookupVar _ [] = Nothing
lookupVar searchVar ((checkVar, resVal):xs) = if searchVar == checkVar
                                                    then (Just resVal)
                                                    else lookupVar searchVar xs


--REMOVAL--
--remove loc pair in store
removeLoc :: Loc -> Store -> Store
removeLoc loc [] = []
removeLoc loc ((iterLoc, value):xs) = if loc == iterLoc 
                                      then xs 
                                      else (iterLoc, value):(removeLoc loc xs)



--remove var pair in env
removeVar :: Var -> Env -> Env
removeVar _ [] = []
removeVar searchVar ((checkVar, resVal):xs) = if searchVar == checkVar
                                                    then xs
                                                    else (checkVar, resVal):(removeVar searchVar xs)
 

--UPDATES--
--update a loc in a store
updateLoc :: Loc -> Value -> Store -> Store
updateLoc searchLoc newVal [] = []
updateLoc searchLoc newVal ((iterLoc, iterVal):xs) = if searchLoc == iterLoc
                                                     then (iterLoc, newVal):xs
                                                     else (iterLoc, iterVal):(updateLoc searchLoc newVal xs)
--update a variable in a env
updateVar :: Var -> Value -> Env -> Env
updateVar searchVar newVal [] = []
updateVar searchVar newVal ((iterVar, iterVal):xs) = if searchVar == iterVar
                                                     then (iterVar, newVal):xs
                                                     else (iterVar, iterVal):(updateVar searchVar newVal xs)


--CONTEXT UPDATES--
--update store in context
updateStore :: Store -> Context -> Context
updateStore newStore context  = context {store = newStore}

--update env in context
updateEnv :: Env -> Context -> Context
updateEnv newEnv context = context {env = newEnv}


--DEPRECATED--
{-
updateEnv :: Context -> Var -> Value -> Context
updateEnv context var val = Context (updateEnvHelper (env context) var val) (store context)

updateEnvHelper :: Env -> Var -> Value -> Env
updateEnvHelper [] searchVar newVal = [(searchVar, newVal)]
updateEnvHelper ((checkVar, checkVal):xs) searchVar newVal = if searchVar == checkVar
                                                               then (checkVar, newVal):xs
                                                               else (checkVar, checkVal):(updateEnvHelper xs searchVar newVal)

-}
