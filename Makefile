main: Lexer.x Parser.y Eval.hs Repl.hs
	alex Lexer.x
	happy Parser.y
	ghc --make Repl.hs
