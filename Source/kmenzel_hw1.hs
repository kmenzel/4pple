import Data.List

--Karl Menzel
--Pledged

--Problem 1
squaresUpTo :: Integer -> [Integer]
squaresUpTo n = [(x*x) | x <- [1..n], (x*x) <= n]

--Problem 2
productTrips :: [Integer] -> [(Integer, Integer, Integer)]
productTrips ns = [(x,y,z) | x <- ns, y <- ns, z <- ns, x < y, (x*y) == z]

--Problem 3
findMultiples :: Integer -> [Integer] -> [Integer]
findMultiples p comps = [x | x <- comps, y <- [1..x], y * p == x]

findMultiplesOfPrimes :: [Integer] -> [Integer] -> [(Integer, [Integer])]
findMultiplesOfPrimes primes mults = [(p, findMultiples p mults) | p <- primes]

--Problem 4
myIntersect :: (Eq a) => [a] -> [a] -> [a]
myIntersect xs ys = let lst1 = [a | a <- xs, a `elem` ys] in 
    [a | a <- ys, a `elem` xs, not (a `elem` lst1)] ++ lst1

--Problem 5
fact :: Int -> Int
fact 0 = 1
fact n = n * (fact (n-1))

--Problem 6
fib :: Integer -> Integer
fib n = aux n 1 0 where
    aux 0 prev1 prev2 = prev1 + prev2
    aux num prev1 prev2 = aux (num-1) (prev1+prev2) prev1

--Problem 7
flatmap :: (t -> [a]) -> [t] -> [a]
flatmap f [] = []
flatmap f (x:xs) = (f x) ++ flatmap f xs

--Problem 8
findBest :: (a -> Int) -> [a] -> a
findBest f (x:[]) = x
findBest f (x:xs) = if (f x) > (f recur) then x else recur
    where recur = findBest f xs

--Warmups
incByN :: (Num a) => [a] -> a -> [a]
incByN lst n = map (+ n) lst

sumlist :: [Integer] -> Integer
sumlist (x:xs) = foldl (+) x xs

exists :: (t -> Bool) -> [t] -> Bool
exists f (x:xs) = foldl (||) False (map f (x:xs))

--Problem 9
multpairs :: (Num a) => [a] -> [a] -> [a]
multpairs xs ys = zipWith (*) xs ys

--Had issues doing it with the proper type
--Problem 10

bucket :: Eq a => (a -> b) -> [a] -> [[a]]
bucket f xs = nup (map (makeSingleBucket f xs) xs) []

nup :: Eq a => [[a]] -> [[a]] -> [[a]]
nup (x:xs) build = if x `elem` build then (nup xs build) else (nup xs (x:build))
nup [] build = build

isDuplicate :: Eq a => [a] -> a -> Bool
isDuplicate lst x = 
    if x `elem` lst then 
        (if x `elem` (delete x lst) then True else False)
    else False

makeSingleBucket :: (a -> b) -> [a] -> a -> [a]
makeSingleBucket f xs x = filter (\m -> (f x) == (f m)) xs


--Problem 11
data FuelEfficiency = MPG Float | LPHK Float deriving (Show)
litersPerGallon = 3.7854
kmPerMile = 1.6093

mpg :: FuelEfficiency -> Float
mpg (MPG x) = x
mpg (LPHK x) = (100 * litersPerGallon) / (x * kmPerMile)

lphk :: FuelEfficiency -> Float
lphk (MPG x) = (100 * litersPerGallon) / (x * kmPerMile)
lphk (LPHK x) = x

--Problem 12

instance Eq FuelEfficiency where
    (==) x y = ((mpg x) == (mpg y))
    (/=) x y = not (x == y)

instance Ord FuelEfficiency where
    (<=) x y = (mpg x) < (mpg y) || (mpg x) == (mpg y)

--Warmup

data BST a = Leaf | Node a (BST a) (BST a) deriving (Show)

insertBST :: Ord a => (a,b) -> BST (a,b) -> BST (a,b)
insertBST x Leaf = Node x Leaf Leaf
insertBST (key, val) (Node (k, v) left right) | key < k = Node (k,v) (insertBST (key,val) left) right
                                              | otherwise = Node (k,v) left (insertBST (key,val) right)

--Problem 13
treeLookup :: Ord a => a -> BST (a,b) -> Maybe b
treeLookup x bst = case (findNode x bst) of
                   Just (Node (key, val) l r) -> Just val
                   Just Leaf -> Nothing
                   Nothing -> Nothing

findNode :: Ord a => a -> BST (a,b) -> Maybe (BST (a,b))
findNode x Leaf = Nothing
findNode x (Node (key, val) l r)  = if x == key then Just (Node (key, val) l r) else
                          if x < key then (findNode x l) else (findNode x r)

--Problem 14
update :: Ord a => a -> b -> BST (a,b) -> BST (a,b)
update key val bst = case (treeLookup key bst) of
                        Nothing -> insertBST (key,val) bst
                        Just _ -> updateNode key val bst

updateNode :: Ord a => a -> b -> BST (a,b) -> BST (a,b)
updateNode key val Leaf = Leaf
updateNode key val (Node (k,v) l r) | key == k = Node (k,val) l r
                                    | key < k = Node (k,v) (updateNode key val l) r
                                    | otherwise = Node (k,v) l (updateNode key val r)
