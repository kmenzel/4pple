baseTenToFive :: Integer -> Integer
baseTenToFive 0 = 0
baseTenToFive x = (x `mod` 5) + (10 * (baseTenToFive (x `div` 5)))

baseFiveToTen :: Integer -> Integer
baseFiveToTen x = aux x 0 
                  where aux 0 acc = 0
                        aux x acc = ((x `mod` 10) * (5 ^ acc)) + (aux (x `div` 10) (acc+1))


baseTenToN :: Integer -> Integer -> Integer
baseTenToN 0 n = 0
baseTenToN x n = (x `mod` n) + (10 * (baseTenToN (x `div` n) n))

baseNToTen :: Integer -> Integer -> Integer
baseNToTen x n = aux x n 0 
                  where aux 0 n acc = 0
                        aux x n acc = ((x `mod` 10) * (n ^ acc)) + (aux (x `div` 10) n (acc+1))


