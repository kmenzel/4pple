module Eval where
import Parser
import Lexer
import Data.Maybe
import DataTypes hiding (Token(..))
import Contexts
import EvalBinOp (evalBinOp)

---------------
------EVALUATOR
---------------

--Statments
evalS :: Context -> Stmt -> Maybe (Value, Context)

evalS context (ExpressionStmt exp) = do
                                        (val, store) <- (evalE context exp)
                                        return (val, (updateStore store context))

evalS context (GlobalVarStmt varName varDef) = case (evalE context varDef) of
                                                     Just (val, store) -> Just (UnitV, (retContext val store))
                                                     Nothing -> Nothing
                                               where retContext val threadStore = let updatedEnv = (addToEnv (varName, val) (env context)) in (Context updatedEnv threadStore) 
                                               

evalS context (RecurFunctStmt xVar exp) = undefined --do let a = (updateEnv env xVar (fromJust (fst b)))
                                                    --b <- (evalS a (RecurFunctStmt xVar exp))
                                                    --(UnitV, (updateEnv a xVar (fromJust (fst b))))

--Expressions
evalE :: Context -> Expr -> Maybe (Value, Store)

evalE context (IntExp x) = Just ((IntV x), (store context))
evalE context (RealExp x) = Just ((RealV x), (store context))
evalE context (BoolExp x) = Just ((BoolV x), (store context))
evalE context (StringExp x) = Just ((StringV x), (store context))
evalE context (VarExp x) = Just ((fromMaybe (StringV ("Variable " ++ (show x) ++ " not found.")) (lookupVar x (env context))), (store context))
evalE context UnitExp = Just (UnitV, (store context))

evalE context (PairExp x y) = do
                            (val1, store1) <- (evalE context x) 
                            (val2, store2) <- (evalE (updateStore store1 context) y)
                            return ((PairV val1 val2), store2)

evalE context (FstExp x) = appMaybeUnary (firstV) (evalE context x)
evalE context (SndExp x) = appMaybeUnary (secondV) (evalE context x)
evalE context (NegateExp x) = appMaybeUnary (negateV) (evalE context x)

--Left off about here updating all the updateStore, updateEnv, context functions
evalE context (FunctExp x y) = Just ((FunctV x y (env context)), (store context))
evalE context (ApplicationExp x y) = do ((FunctV varName functBody declerationEnv), store) <- (evalE context x)
                                        (argumentVal, newStore) <- (evalE (updateStore store context) y)
                                        let retContext = (Context (addToEnv (varName, argumentVal) declerationEnv) newStore)
                                        (evalE retContext functBody)
                                   
evalE context (BinOpExp a op b) = do
                                (val1, newStore) <- (evalE context a)
                                (val2, newerStore) <- (evalE (updateStore newStore context) b)
                                res <- (evalBinOp op val1 val2)
                                return (res, newerStore)

evalE context (IfThenElseExp a b c) = case (evalE context a) of
                                        Nothing -> Nothing
                                        Just (condition, store) -> if (condition == (IntV 1) || (condition == (RealV 1.0)) || condition == (BoolV True))
                                                                    then (evalE (updateStore store context) b)
                                                                    else (evalE (updateStore store context) c)

evalE context (LocalVarExp varName varDefExpr inExpr) = case (evalE context varDefExpr) of
                                                        Just (varDef, store) -> (Context (addToEnv (varName, varDef) (env context)) store) `evalE` inExpr
                                                        Nothing -> Nothing

evalE context (SeqExp a b) = do
                                (val, store) <- (evalE context a)
                                (evalE (updateStore store context) b)

evalE context (RefExp a) = do
                            (val, store) <- (evalE context a)
                            let newLoc = freshLoc store
                            let newStore = addToStore (newLoc, val) store
                            return ((LocV newLoc), newStore)

evalE context (DeRefExp a) = case evalE context a of
                                Just ((LocV x), store) -> case lookupLoc x store of
                                                            Just val -> Just (val, store)
                                                            Nothing -> Nothing
                                _ -> Nothing

evalE context (AssignExp a b) = case evalE context a of
                                    Just ((LocV x), store') -> case (evalE (context {store = store'}) b) of
                                                                    Just (val, store'') -> Just (UnitV, (addToStore (x, val) (removeLoc x store'')))
                                                                    Nothing -> Nothing
                                    _ -> Nothing

evalE context (WhileExp a b) = case (evalE context a) of
                                Just ((BoolV False), store') -> Just (UnitV, store')
                                Just ((BoolV True), store') -> evalE (context {store = store'}) (WhileExp a b)
                                _ -> Nothing

---------------------
------UNARY OPERATORS
------Title: Unary Value Fuctions
--Note that these value functions take a Value instead 
--of a Maybe Value. Where these functions are called in evalE has
--to use the special maybe handeling fuction. While the Maybe Value
--produced by the recursive call in evalE could have been handeled
--in these value functions, their type signatures would become inconsistent
--with the type signatures of the other value fuctions and the purpose for
--having the value fuction return a maybe at all would be obscured. The 
--real purpose is to catch the possibility of being passed the improper 
--value type (a type error) as opposed to a the passed value not being of
--type PairV.

appMaybeUnary :: (Value -> Maybe Value) -> Maybe (Value, Store) -> Maybe (Value, Store)
appMaybeUnary op evalERes = do 
                                (val, store) <- evalERes
                                resVal <- (op val)
                                return (resVal, store)

firstV :: Value -> Maybe Value
firstV (PairV x _) = Just x
firstV _ = Nothing

secondV :: Value -> Maybe Value
secondV (PairV _ x) = Just x
secondV _ = Nothing

negateV :: Value -> Maybe Value
negateV (IntV x) = Just $ IntV $ -x
negateV (RealV x) = Just $ RealV $ -x
negateV (BoolV x) = Just $ BoolV $ not x
negateV _ = Nothing
